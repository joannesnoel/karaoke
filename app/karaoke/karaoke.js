'use strict';

angular.module('myApp.karaoke', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/karaoke', {
    templateUrl: 'karaoke/karaoke.html',
    controller: 'KaraokeCtrl'
  });
}])

.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
})

.service('Playlist', [function(){
	var Playlist = [];
	return {
		list: Playlist,
		reserve: function(song){
			Playlist.push(song);
			if(Playlist.length == 1 && (player.ended() || player.src() == ''))
				this.next();
		},
		next: function(){
			if(Playlist.length > 0){
				var song = Playlist.splice(0,1)[0];

				player.pause();
				player.currentTime(0);
				
				var data = {
					type: "video/mp4",
					src: server + song.id + ".mp4"
				}

				player.src(data);
				
				player.ready(function() {
					this.one('loadeddata', videojs.bind(this, function() {
						this.currentTime(0);
					}));

					this.load();
					this.play();
				});
			}
		}
	}
}])

.service('SongSearch', ['$http', function($http){
	var Songs = [];
	var compare = function compare(a,b) {
	  if (a.sort < b.sort)
	     return -1;
	  if (a.sort > b.sort)
	    return 1;
	  return 0;
	}
	return {
		all: Songs,
		find: function(str){
			Songs.length = 0;
			if(str.length < 5)
				return;
			var url = "https://www.googleapis.com/youtube/v3/search?key=AIzaSyBCz6mcgoseMhISuWzFXxLtTSLt3OzZq7o&q=" + str + "+karaoke&type=video&part=id,snippet&order=relevance&maxResults=50";
			//canceler.resolve();
			var items = [];
			$http.get(url)
			.success(function(data, status, headers, config){
				items = items.concat(data.items);
				console.log(items.length);
				$http.get(url + '&pageToken=' + data.nextPageToken)
				.success(function(data, status, headers, config){
					items = items.concat(data.items);
					console.log(items.length);
					angular.forEach(items, function(elem){
						var skip = false;
						var words = str.split(/\s/);
						angular.forEach(words, function(word){
							if(skip == false && elem.snippet.title.toLowerCase().indexOf(word.toLowerCase()) < 0)
								skip = true;
						});
						if(elem.snippet.title.regexIndexOf(/[^a-z0-9]demo[^a-z0-9]/i) < 0 && skip == false){
							var item = {};
							if(elem.snippet.channelTitle == 'KaraokeOnVEVO' ||
								elem.snippet.channelTitle == 'YouSingTheHits' ||
								elem.snippet.channelTitle == 'singkingkaraoke' ||
								elem.snippet.channelTitle == 'karafun' ||
								elem.snippet.channelTitle.toLowerCase().indexOf('KaraokeMedia') > -1){
								item.sort = 1;
							}else if(elem.snippet.channelTitle == 'AnywhereKY' ||
								elem.snippet.channelTitle == 'MURAMATSUKARAOKE' ||
								elem.snippet.channelTitle == 'jimmyzkaraoke' ||
								elem.snippet.channelTitle == 'customizedkaraoke' ||
								elem.snippet.channelTitle == 'singalongtv' ||
								elem.snippet.channelTitle == 'xxnognogxx' ||
								elem.snippet.channelId == 'UCRoAoGqqLuOIWztkcxUiYoA'){
								item.sort = 2;
							}
							if(item.sort){
								item.id = elem.id.videoId;
								item.title = elem.snippet.title;
								item.channel = elem.snippet.channelTitle;
								Songs.push(item);
							}
						}
					});
				});
				Songs.sort(compare);
			})
		}
	}
}])

.controller('KaraokeCtrl', ['$scope','$http','$routeParams','SongSearch','Playlist',function($scope, $http, $routeParams, SongSearch, Playlist) {
	//http://www.youtube.com/get_video_info?video_id=Iv-Xmv2yjjQ&el=vevo&el=embedded&asv=3&sts=15902
	player = videojs('video-screen', {
		techOrder: ['flash', 'html5'],
		autoplay: true,
		// sources: [{ 
		// 	type: "video/flv",
		// 	src: server + 'szROVj0Swcs.mp4'
		// }]
	});

	player.on("ended", Playlist.next);

	$scope.find = function(){
		SongSearch.find($scope.searchKey);
	}

	$scope.addToPlaylist = function(song){	
		$scope.status = "adding song to playlist... please wait";
		$http.post('http://creative-logik.com:8080/video', { id: song.id })
		.success(function(video_url, status, headers, config){
			Playlist.reserve(song);
			$scope.status = "";
		});
		$scope.searchKey = "";
		SongSearch.find($scope.searchKey);
	}

	$scope.next = Playlist.next;

	$scope.results = SongSearch.all;
	$scope.playlist = Playlist.list;
	
}]);